---
title: DAV-Felsinfo data as GPX track (or other formats)
date: 2018-08-12
resources:
- name: description
  src: description.png
  title: Description of a single rock with the link to the full data
- name: map-overview
  src: map-overview.png
  title: All rocks are displayed in the normal map view
- name: gpx-file
  src: felsinfo.gpx
---
We want to go climbing...

The [website of the DAV](http://felsinfo.alpenverein.de/) has a nice list of rocks with descriptions and coordinates, but is not optimized for mobile/offline use.
Ideally I would like to have the list of all rocks in my navigation app on my phone ([OsmAnd](https://osmand.net/) in my case).
Thus I wrote a small script which scapes their website and generates a [GPX](https://en.wikipedia.org/wiki/GPS_Exchange_Format) file with the rocks.

The website is parsed using [Nokogiri](http://www.nokogiri.org/):

```ruby
def read_page(url)
    page = Nokogiri::HTML(open(url))
    /(?<name>Fels.*)/ =~ page.css('title').text
    info_text = page.css('#allgemein > div .kategorie_titel,.felsinfo').text
    /Geogr.\ Länge\s*(?<longitude>\d+\.\d+)\s*°/m =~ info_text
    /Geogr.\ Breite\s*(?<latitude>\d+\.\d+)\s*°/m =~ info_text
    {
        name: name,
        url: 'http://%s' % url,
        latitude: latitude,
        longitude: longitude,
    }
end
```

And the result is passed to rubys templating engine:

```ruby
def template_binding(rocks)
    include ERB::Util
    binding
end

puts ERB.new(File.read(options[:template])).result(template_binding(rocks))
```

This allows to define arbitrary templates. In my case I am mostly interested into GPX:
```xml
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" creator="Wikipedia"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
    <% for @rock in rocks.select { |x| x[:latitude] != nil } %>
        <wpt lat="<%= @rock[:latitude] %>" lon="<%= @rock[:longitude] %>">
            <name><%= html_escape(@rock[:name]) %></name>
            <cmt><%= html_escape(@rock[:url]) %></cmt>
        </wpt>
    <% end %>
</gpx>
```

Loaded into OsmAnd this allows me to view all rocks on the map as well as access the original page with the full info:

<div class="flex">
{{< img "map-overview" >}}
{{< img "description" >}}
</div>

In the end, [here]({{< file "gpx-file" >}}) is a GPX file including all the rocks. The code is available at https://gitlab.com/ablu/felsinfo-exporter.